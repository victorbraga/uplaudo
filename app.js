const {google} = require("googleapis");
const path = require('path');
const fs = require('fs');
const { auth } = require("googleapis/build/src/apis/abusiveexperiencereport");
const { file } = require("googleapis/build/src/apis/file");

const CLIENT_ID = '275854889210-hvnivtrr0ic1jv50u3psjd7484l261rk.apps.googleusercontent.com';
const CLIENT_SECRET = 'bAniCjFHnPuxwkMiaQJGYM4u';
const REDIRECT_URI = 'https://developers.google.com/oauthplayground';

const REFRESH_TOKEN = '1//04XBtgHms_4tuCgYIARAAGAQSNwF-L9IrlXfNDBp-yezQRtKyhiV4J6hDBUU6kxkigA6XgozQXYaDhLOrApjrgLNAkpcyd4u52-o';

const oauth2Client = new google.auth.OAuth2(
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URI
);

oauth2Client.setCredentials({refresh_token: REFRESH_TOKEN});

const drive = google.drive({
    version: 'v3',
    auth: oauth2Client
})

const filePath = path.join(__dirname, 'CVA000003.pdf');

async function uploadFile(){
    try {
        const response = await drive.files.create({
            requestBody:{
                name:'up.pdf',
                mimeType: 'application/pdf'
            },
            media:{
                mimeType: 'application/pdf',
                body: fs.createReadStream(filePath)
            }
        })
        console.log(response.data);
    } catch (error) {
        console.log(error.message)
    }
}

async function deleteFile(){
    try {
        const response = await drive.files.delete({
            fileId: '1l19LYIbcUBEYP4B5ufJfi-iqieLM3EG8'
        });
        console.log(response.data, response.status);
    } catch (error) {
        console.log(error.message);
    }
}

async function generatePublicUrl(){
    try {
        const fileId = '1l19LYIbcUBEYP4B5ufJfi-iqieLM3EG8'
        await drive.permissions.create({
            fileId: fileId,
            requestBody:{
                role:'reader',
                type: 'anyone'
            }
        })
        const result = await drive.files.get({
            fileId: fileId,
            fields:'webViewLink, webContentLink'
        })
        console.log(result.data);
    } catch (error) {
        console.log(error.message);
    }
}

async function downloadFile(){
    const fileId = '1l19LYIbcUBEYP4B5ufJfi-iqieLM3EG8';
    const dest = fs.createWriteStream('./tmp/laudo.pdf');
    
    const result = await drive.files.get({
        fileId, alt:'media'
    },{
        responseType:'stream'
    });
    try {
        result.data.pipe(dest);
    } catch (error) {
        console.log(error);
    }
}

// deleteFile();
// uploadFile();
// generatePublicUrl();
downloadFile();




